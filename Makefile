.PHONY: html pdf docx rtf

all: html pdf docx rtf

pdf: resume.pdf
resume.pdf: style.tex resume.md
	pandoc --standalone --template style.tex \
	--from markdown --to context \
	-V papersize=A4 \
	-o resume.tex resume.md; \
	context resume.tex

martino_visintin.pdf: resume.pdf
	cp resume.pdf martino_visintin.pdf

html: resume.html
resume.html: style.css resume.md
	pandoc --standalone --css style.css \
        --from markdown --to html \
        -o resume.html resume.md

docx: resume.docx
resume.docx: resume.md
	pandoc -s resume.md -o resume.docx

rtf: resume.rtf
resume.rtf: resume.md
	pandoc -s resume.md -o resume.rtf

clean:
	rm -f resume.html
	rm -f resume.tex
	rm -f resume.tuc
	rm -f resume.log
	rm -f resume.pdf
	rm -f resume.docx
	rm -f resume.rtf
	rm -f martino_visintin.pdf

watch:
	echo "resume.md" | entr -- make
