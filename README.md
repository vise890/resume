# Martino's Resume

### [`pdf`](https://vise890.gitlab.io/resume/resume.pdf) | [`html`](https://vise890.gitlab.io/resume/resume.html) | [`docx`](https://vise890.gitlab.io/resume/resume.docx) | [`rtf`](https://vise890.gitlab.io/resume/resume.rtf)

## Building locally

```bash
sudo pacman -S pandoc texlive-core
make
```

### Live preview

Install [Live Server](https://ritwickdey.github.io/vscode-live-server/) for
VSCode. Then:

```bash
watchexec -- 'make clean && make html'
```
