# Martino Visintin

> [martino@visint.in](mailto:martino@visint.in) · [martino.visint.in](https://martino.visint.in) · [gitlab](https://gitlab.com/vise890)

## Experience

**Health Unlocked** | _Senior Software Engineer_ | June 2021 – November 2022 | Remote

- Extracted several features from legacy .NET monolith to Clojure-based
  service;

- Operationalised email sending ecosystem, migrated several microservices to
  it;

- Introduced Grafana and Prometheus for monitoring critical services (main API,
  email sending ecosystem).

**Trenolab** | _Senior Software Developer_ | May 2019 – May 2021 | Gorizia, Italy

- Built interactive visualisations for train timetables (Typescript/d3)

- Implemented application to compare metrics across different software versions
  and detect regressions. Application now used for refactoring in-house
  simulator software;

- Contributed to the design, implementation and operation of web application to
  track and escalate anomalies in ferrous cargo (Java, Spring Boot,
  PostgreSQL). Application now used by client;

- Ingested, processed and aggregated train movement data to extract more
  accurate train timings (PostgreSQL) for a UK client.

**OVO Energy** | _Software Engineer_ | Apr 2016 – Apr 2019 | Bristol, UK

- Developed a generic parser for Energy Industry data to make it human-readable
  and queryable. Project now used by multiple microservices in production and
  for ad hoc business analysis;

- Set-up infrastructure for building, testing and deploying micro-services for
  several teams. This included: CI/CD pipelines (CircleCI, Jenkins, Concourse,
  GoCD); monitoring and alerting (Prometheus); dashboards (Grafana); automating
  deployments (Kubernetes and Helm on Google Cloud Platform, AWS Elastic
  Beanstalk with Terraform);

- Worked with teams to spin up several micro-services with both Scala and
  Clojure interfacing with Kafka, GCP DataStore, GCP PubSub and legacy systems.

**MixRadio** | _Senior Software Engineer_ | January - April 2016 | Bristol, UK

- MixRadio closed down in April 2016, shortly after I joined. During my brief
  time there I extended, tested and refactored existing micro-services in
  Clojure.

**Avaaz** | _Software Developer_ | April - June 2015 | Remote

- Extracted Python/Flask micro-services from PHP monolith.

**The Recurse Center** | January - March 2015 | NYC, USA

- Built several personal projects with Haskell and Clojure.

**ThoughtWorks** | _Software Developer_ | January 2014 - March 2015 | London, UK

- Worked for private and public clients in a variety of technologies.

## Education

**University of Strathclyde** | 2008 - 2013 | Glasgow, UK

- **BEng(Hons), First Class** in _Structural and Architectural Engineering_

- Worked in interdisciplinary teams (Mechanical and Civil Engineers and
  Architects);

- Experience with: LaTeX, CAD, ESP-r (energy modeling);

- **Dissertation** _Energy demand reduction through architectural design_.
  Designed and built thermodynamic model for a cabin. Simulated its energy
  performance and human comfort characteristics. Iterated the design by
  integrating low energy design strategies.

**McMaster University** | 2010 - 2011 | Hamilton, Canada

**United World College of South East Asia** | 2006 - 2008 | Singapore

## Languages

Italian – English – Spanish (limited proficiency)
